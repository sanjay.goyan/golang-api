BINARY_NAME=sticker-app

download:
	go mod download
test-v:
	go test -v -cover -covermode=atomic ./...
test:
	go test -cover -covermode=atomic ./...
build:
	go build -o ${BINARY_NAME} app/*.go
run: build
	./${BINARY_NAME}

docker-build:
	docker-compose up --build --no-start
start-mysql: docker-build
	docker-compose start mysql
start-app: docker-build
	docker-compose start apps
restart-mysql:
	docker-compose restart mysql
restart-app: docker-build
	docker-compose restart app
stop-app:
	docker-compose stop app
stop-mysql:
	docker-compose stop mysql
