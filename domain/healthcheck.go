package domain

// region Response Structs
type HealthCheck struct {
	Status  string  `json:"status"`
	Version string  `json:"version"`
	Checks  []Check `json:"checks"`
}

// Check struct is used for returning state of particular service
type Check struct {
	Name  string `json:"name"`
	State string `json:"state"`
}
// endregion

// region Domain Interfaces
type HealthCheckUseCase interface {
	CheckDBConnection() (err error)

}

type HealthCheckRepository interface {
	CheckDBConnection() (err error)
}
// endregion
