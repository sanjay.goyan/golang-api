package domain

import "time"

// region Table Names

const (
	StickerTable = "stickers"
)

// endregion

// region tables Structs

type Sticker struct {
	ID              uint      `json:"id"`
	Url             string    `json:"url"`
	DisplayTimeFrom string    `json:"-"`
	DisplayTimeTo   string    `json:"-"`
	UpdatedAt       time.Time `json:"-"`
	CreatedAt       time.Time `json:"-"`
}

func (sticker *Sticker) TableName() string {
	return StickerTable
}

// endregion

// region Response Structs

//type TrendingStickersRes struct {
//	TrendingStickers []Sticker `json:"trendingStickers"`
//}

// endregion

// region interface Structs

type StickerUseCase interface {
	GetTrendingStickers() ([]*Sticker, error)
}

type StickerRepository interface {
	GetTrendingStickers(currentTime string) ([]*Sticker, error)
}

// endregion
