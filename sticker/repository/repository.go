package mysql

import (
	"sticker-app/domain"
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

func NewSticker(db *gorm.DB) domain.StickerRepository {
	return &repository{db: db}
}

// region Sticker

func (repository repository) GetTrendingStickers(currentTime string) ([]*domain.Sticker, error) {
	trendingStickers := make([]*domain.Sticker, 0)
	err := repository.db.Table("stickers").
		Where("stickers.display_time_from <= ?", currentTime).
		Where("stickers.display_time_to >= ?", currentTime).
		Find(&trendingStickers).Error

	return trendingStickers, err
}

// end region