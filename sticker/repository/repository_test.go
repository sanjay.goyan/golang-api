package mysql

import (
	"gorm.io/driver/mysql"
	"sticker-app/config"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gorm.io/gorm"
	"sticker-app/domain"
)

// Test code for Sticker Repository
func TestGetTrendingStickers(t *testing.T) { // Testing the Fetch function with all trending stickers

	DB, mock, err := sqlmock.New()

	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	dialector := mysql.New(mysql.Config{
		DSN:                       "sqlmock_db_0",
		DriverName:                "mysql",
		Conn:                      DB,
		SkipInitializeWithVersion: true,
	})
	db, err := gorm.Open(dialector, &gorm.Config{})


	defer DB.Close()

	mockStickers := []*domain.Sticker{
		{
			ID: 1, Url: "name1", DisplayTimeFrom: "11:00:00",
			DisplayTimeTo: "20:00:00", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}

	rows := sqlmock.NewRows([]string{"id", "name", "url", "time_slot_start", "time_slot_end", "created_at"}).
		AddRow(mockStickers[0].ID, mockStickers[0].Url, mockStickers[0].DisplayTimeFrom, mockStickers[0].DisplayTimeTo,
			mockStickers[0].CreatedAt, mockStickers[0].UpdatedAt)

	query := "[SELECT * FROM sticker DESC LIMIT ?]"

	mock.ExpectQuery(query).WillReturnRows(rows)

	now := time.Now()
	currentTime := now.Format(config.TimeLayout)

	a := NewSticker(db)
	list, err := a.GetTrendingStickers(currentTime)

	assert.NoError(t, err)
	assert.Len(t, list, 1)

}

