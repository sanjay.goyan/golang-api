package usecase

import (
	"sticker-app/config"
	"sticker-app/domain"
	"time"
)

type useCase struct {
	repository domain.StickerRepository
}

func NewSticker(repository domain.StickerRepository) domain.StickerUseCase {
	return &useCase{repository: repository}
}

// region Sticker

func (useCase useCase) GetTrendingStickers() (result []*domain.Sticker, err error) {
	now := time.Now()
	currentTime := now.Format(config.TimeLayout)

	stickers,err := useCase.repository.GetTrendingStickers(currentTime)

	for _, sticker := range stickers {
		result = append(result	, sticker)
	}
	return
}

// end region