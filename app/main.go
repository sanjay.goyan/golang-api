package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"sticker-app/config"
	healthCheckDelivery "sticker-app/healthcheck/delivery/http"
	"sticker-app/healthcheck/repository"
	healthCheckUseCase "sticker-app/healthcheck/usecase"

	stickerDelivery "sticker-app/sticker/delivery/http"
	stickerRepository "sticker-app/sticker/repository"
	stickerUseCase "sticker-app/sticker/usecase"
)

var (
	e *echo.Echo
)

func init() {
	// Initialise echo context for routes
	e = echo.New()
	e.Use(middleware.Logger())
	config.InitializeConfig()
}

func main() {
	// Load database config from config.yml/environment
	databaseConfig, err := config.GetDbConfig()
	if err != nil {
		e.Logger.Fatal(err)
	}

	// Establish data base connection
	db, err := gorm.Open(mysql.Open(databaseConfig.DbURL), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		e.Logger.Fatal(err)
	}
	fmt.Println(db)
	// Load application config from config.yml/environment
	appConfig, err := config.GetAppConfig()
	if err != nil {
		e.Logger.Fatal(err)
	}

	sqlDB, err := db.DB()
	defer sqlDB.Close()

	// health check
	healthCheckDelivery.NewHealthCheck(e, healthCheckUseCase.NewHealthCheck(repository.NewHealthCheck(db)))

	// trending stickers
	stickerDelivery.NewSticker(e, stickerUseCase.NewSticker(stickerRepository.NewSticker(db)))

	log.Fatal(e.Start(":" + appConfig.Port))
}
