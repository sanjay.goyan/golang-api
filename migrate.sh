#!/bin/bash

DB_USERNAME: "root"
DB_PASSWORD: "123456"
DB_NAME: "sticker_apps"
DB_HOST: "localhost"
DB_PORT: "3307"


db_name=$(mysql -u $DB_USERNAME -p$DB_PASSWORD -se "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '${DB_NAME}'")
 
if [ "$db_name" != "$DB_NAME" ]; then
    mysql -u ${DB_USERNAME} -p${DB_PASSWORD} -Bse "CREATE DATABASE \`${DB_NAME}\`;" && \
	zcat ../users.sql.gz | mysql -u ${DB_USERNAME} -p${DB_PASSWORD} ${DB_NAME}	
fi
 