package errors

// ErrorDetails is a struct used for storing response of error details
type ErrorDetails struct {
	Code        string `json:"errorCode"`
	Description string `json:"errorDescription"`
}

func (ed ErrorDetails) Error() string {
	return ed.Code
}


// following are the errors we will be returning in the response
var(
	UnexpectedError = ErrorDetails{Code: "unexpectedError", Description: "An unexpected error occurred. Please try again later"}

)
