CREATE TABLE IF NOT EXISTS `stickers` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL COMMENT 'Url of sticker image ',
  `display_time_from` time NULL COMMENT 'Display time when enabled to show on front end',
  `display_time_to` time NULL COMMENT 'Display time when disabled to show on front end ',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp at which the row was updated',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp at which the row was created'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `stickers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_display_time_from` (`display_time_from`),
  ADD KEY `ix_display_time_to` (`display_time_to`);

