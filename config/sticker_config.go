package config

import "github.com/spf13/viper"

const TimeLayout = "15:04:05"

const (
	//StatusUp represents Service Status Up
	StatusUp = "UP"
	//StatusDown represents Service Status Down
	StatusDown = "DOWN"
	//Success represents Success Status
	Success = "success"
	//Failure represents Failure Status
	Failure = "failure"
	//Database represents database
	Database = "database"
	//Version represents version of the latest tag on gitlab
	Version = "2.7.0"
)

func InitializeConfig() {

	// Set configuration file which will be used to get/set config values
	viper.SetConfigFile(`config.yml`)
	// Ask viper to overwrite any configuration values with their corresponding environment counterparts
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}