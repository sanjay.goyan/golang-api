package config

import (
	"github.com/spf13/viper"
)

// AppConfiguration represent configuration for this application
type AppConfiguration struct {
	Environment string `mapstructure:"APPLICATION_ENVIRONMENT"`
	Port        string `mapstructure:"APPLICATION_PORT"`
}

// GetAppConfig will load the application config
func GetAppConfig() (appConfig AppConfiguration, err error) {
	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	// Load the app configuration in the AppConfiguration struct
	err = viper.Unmarshal(&appConfig)
	if err != nil {
		return
	}

	return
}
