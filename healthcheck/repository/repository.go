package repository

import (
	"gorm.io/gorm"
	"sticker-app/domain"
)

type repository struct {
	db *gorm.DB
}

func NewHealthCheck(db *gorm.DB) domain.HealthCheckRepository {
	return &repository{db: db}
}

func (repository repository) CheckDBConnection() (err error) {

	db, err := repository.db.DB()
	if err != nil {
		return
	}
	err = db.Ping()
	return
}
