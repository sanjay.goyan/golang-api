package usecase

import "sticker-app/domain"

type useCase struct {
	repository domain.HealthCheckRepository
}

func NewHealthCheck(repository domain.HealthCheckRepository) domain.HealthCheckUseCase {
	return &useCase{repository: repository}
}

//CheckDBConnection check the health status of the DB instance
func (useCase useCase) CheckDBConnection() (err error) {
	err = useCase.repository.CheckDBConnection()
	return
}