package http

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"sticker-app/config"
	"sticker-app/domain"
)

type delivery struct {
	useCase domain.HealthCheckUseCase
}

func NewHealthCheck(e *echo.Echo, useCase domain.HealthCheckUseCase) {
	handler := &delivery{
		useCase: useCase,
	}

	e.GET("/v1/healthCheck", handler.HealthCheck)
}

func (delivery *delivery) HealthCheck(ctx echo.Context) error {
	var status = config.Success
	var dbStatus = config.StatusUp

	err := delivery.useCase.CheckDBConnection()
	if err != nil {
		status = config.Failure
		dbStatus = config.StatusDown
	}

	healthCheckTypes := []domain.Check{
		{Name: config.Database, State: dbStatus},
	}
	checks := AddhealthCheckTypes(healthCheckTypes)
	response := &domain.HealthCheck{
		Status:  status,
		Version: config.Version,
		Checks:  checks,
	}
	if err != nil {
		return ctx.JSON(http.StatusServiceUnavailable, response)
	}
	return ctx.JSON(http.StatusOK, response)
}
